/*
 *	CUSTOM JAVASCRIPT
 */

$j=jQuery.noConflict();

// Use jQuery via $j(...)
$j(document).ready(function(){
	var pathname = window.location.pathname;
	var navBlog = jQuery("#menu-item-24");
	if (pathname.indexOf("blog") >= 0) {
		navBlog.addClass("active");
	}

	$j('.dropdown').hover(function() {
        $j(this).addClass('open');
    }, function() {
        $j(this).removeClass('open');
    });
});