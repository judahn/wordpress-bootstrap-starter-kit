<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>
	<div id="root-footer"></div>
</div>


<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="row">
			
			<div class="site-info">
				&copy; <?php echo date("Y"); ?> <a class="footer-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> All rights reserved.
			</div><!-- close .site-info -->
			
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->

<?php wp_footer(); ?>

</body>
</html>